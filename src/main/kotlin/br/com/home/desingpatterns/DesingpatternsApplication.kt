package br.com.home.desingpatterns

import br.com.home.desingpatterns.creationals.abstractfactory.services.DeliveryCostCalculator
import br.com.home.desingpatterns.structurals.adapter.clients.TemperatureReader
import br.com.home.desingpatterns.creationals.builder.services.PizzaRecipeCreator
import br.com.home.desingpatterns.creationals.factorymethod.services.DeliveryTimeCalculator
import br.com.home.desingpatterns.structurals.bridge.clients.ReportRender
import br.com.home.desingpatterns.structurals.composite.clients.CardStatusReader
import br.com.home.desingpatterns.structurals.decorator.clients.CostCalculator
import br.com.home.desingpatterns.structurals.proxy.clients.BookAnalyzer
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DesingpatternsApplication

fun main(args: Array<String>) {
	runApplication<DesingpatternsApplication>(*args)

	//Factory Method
	val deliveryTimeCalculator = DeliveryTimeCalculator()
	deliveryTimeCalculator.evaluateDeliveryTime()

	//Abstract Factory
	val deliveryCostCalculator = DeliveryCostCalculator()
	deliveryCostCalculator.evaluateDeliveryCost()

	//Builder
	val pizzaRecipeCreator = PizzaRecipeCreator()
	pizzaRecipeCreator.createPizzas()

	//Adapter
	val temperatureReader = TemperatureReader()
	temperatureReader.readActualTemperature()

	//Proxy
	val bookAnalyzer = BookAnalyzer()
	bookAnalyzer.analyzeBook()

	//Decorator
	val costCalculator = CostCalculator()
	costCalculator.evaluateTotalCost()

	//Composite
	val cardStatusReader = CardStatusReader()
	cardStatusReader.printCardsProgress()

	//Bridge
	val reportRender = ReportRender()
	reportRender.renderReportData()
}
