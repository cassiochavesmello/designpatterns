package br.com.home.desingpatterns.structurals.proxy.clients

import br.com.home.desingpatterns.structurals.proxy.services.BookParser
import br.com.home.desingpatterns.structurals.proxy.services.LazyCachedBookParserProxy

class BookAnalyzer {

    fun analyzeBook() {
        val bookParser = BookParser("BookName")
        println("Num Pages without caching: " + bookParser.getNumPages())

        val cachedBookParser = LazyCachedBookParserProxy("BookName")
        println("Num Pages with caching: " + bookParser.getNumPages())
    }
}