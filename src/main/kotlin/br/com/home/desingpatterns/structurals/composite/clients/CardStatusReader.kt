package br.com.home.desingpatterns.structurals.composite.clients

import br.com.home.desingpatterns.structurals.composite.models.Card
import br.com.home.desingpatterns.structurals.composite.models.CardGroup
import br.com.home.desingpatterns.structurals.composite.models.CardTypeEnum

class CardStatusReader {

    fun printCardsProgress() {
        val story1 = Card("First story", CardTypeEnum.STORY)
        val story2 = Card("Second story", CardTypeEnum.STORY)
        val story3 = Card("Third story", CardTypeEnum.STORY)
        val epic1 = CardGroup("First Epic", CardTypeEnum.EPIC)
        val epic2 = CardGroup("Second Epic", CardTypeEnum.EPIC)
        val mvp = CardGroup("First MVP", CardTypeEnum.MVP)

        epic1.addCardList(story1)
        epic1.addCardList(story2)
        epic2.addCardList(story3)
        mvp.addCardList(epic1)
        mvp.addCardList(epic2)

        printCardGroup(epic1)
        printCardGroup(mvp)

        story1.setDone()

        printCardGroup(epic1)
        printCardGroup(mvp)
    }

    private fun printCardGroup(cardGroup: CardGroup) {
        println("Percentage Done of " + cardGroup.cardName + " is " + cardGroup.getPercentageDone() + "%")
    }
}