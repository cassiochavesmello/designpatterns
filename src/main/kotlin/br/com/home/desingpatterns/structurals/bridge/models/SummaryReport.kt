package br.com.home.desingpatterns.structurals.bridge.models

class SummaryReport(override val resource: Resource): Report(resource) {

    override fun render() {
        println("SummaryReport =>  Sector: " + resource.getSectorName() + " | " + " Data: " + resource.getActualData())
    }
}