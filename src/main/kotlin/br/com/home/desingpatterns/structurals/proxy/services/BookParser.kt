package br.com.home.desingpatterns.structurals.proxy.services

import br.com.home.desingpatterns.structurals.proxy.clients.IBookParser

class BookParser(val bookName: String): IBookParser {

    override fun getNumPages(): Int {
        return 5
    }
}