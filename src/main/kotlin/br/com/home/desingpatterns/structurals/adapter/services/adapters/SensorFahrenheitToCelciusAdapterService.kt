package br.com.home.desingpatterns.structurals.adapter.services.adapters

import br.com.home.desingpatterns.structurals.adapter.clients.ITemperatureSensorService
import br.com.home.desingpatterns.structurals.adapter.services.SensorFahrenheitService

class SensorFahrenheitToCelciusAdapterService(
    private val sensorFahrenheitService: SensorFahrenheitService
): ITemperatureSensorService {
    override fun getCelsiusTemperature(): Double {
        val temperatureFahrenheit = sensorFahrenheitService.getSensorTemperature()
        return (temperatureFahrenheit - 32)*(5.0/9.0)
    }

}