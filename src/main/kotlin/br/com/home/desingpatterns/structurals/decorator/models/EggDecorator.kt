package br.com.home.desingpatterns.structurals.decorator.models

class EggDecorator(food: IFood) : AddonDecorator(food) {
    override fun getCost(): Double {
        return super.getCost() + 2.50
    }
}