package br.com.home.desingpatterns.structurals.decorator.models

abstract class AddonDecorator(food: IFood) : IFood {
    var foodWrappee : IFood = food

    override fun getCost(): Double {
        return this.foodWrappee.getCost()
    }
}