package br.com.home.desingpatterns.structurals.bridge.models

class AnalyticalReport(override val resource: Resource): Report(resource) {

    override fun render() {
        println("AnalyticalReport =>  Sector: " + resource.getSectorName() + " | " +
                " Data: " + resource.getActualData() + " | " + resource.getHistoricalData())
    }
}