package br.com.home.desingpatterns.structurals.adapter.clients

interface ITemperatureSensorService {
    fun getCelsiusTemperature(): Double
}