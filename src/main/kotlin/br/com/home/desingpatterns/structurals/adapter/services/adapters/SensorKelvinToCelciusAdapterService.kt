package br.com.home.desingpatterns.structurals.adapter.services.adapters

import br.com.home.desingpatterns.structurals.adapter.clients.ITemperatureSensorService
import br.com.home.desingpatterns.structurals.adapter.services.SensorKelvinService

class SensorKelvinToCelciusAdapterService(
    private val sensorKelvinService: SensorKelvinService
): ITemperatureSensorService {
    override fun getCelsiusTemperature(): Double {
        val temperatureKelvin = sensorKelvinService.getSensorTemperature()
        return temperatureKelvin - 273.15
    }

}