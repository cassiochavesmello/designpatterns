package br.com.home.desingpatterns.structurals.bridge.models

abstract class Report(open val resource: Resource) {
    abstract fun render()
}