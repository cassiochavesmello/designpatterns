package br.com.home.desingpatterns.structurals.bridge.models

interface Resource {
    fun getSectorName(): String
    fun getActualData(): String
    fun getHistoricalData(): String
}