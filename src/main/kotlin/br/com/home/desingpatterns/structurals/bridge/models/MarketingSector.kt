package br.com.home.desingpatterns.structurals.bridge.models

class MarketingSector: Resource {

    override fun getSectorName(): String {
        return "Marketing"
    }

    override fun getActualData(): String {
        return "New data from marketing sector"
    }

    override fun getHistoricalData(): String {
        return "Historical data from marketing sector"
    }
}