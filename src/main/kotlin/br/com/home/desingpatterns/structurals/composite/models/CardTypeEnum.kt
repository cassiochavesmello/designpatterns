package br.com.home.desingpatterns.structurals.composite.models

enum class CardTypeEnum {
    MVP,
    EPIC,
    STORY
}