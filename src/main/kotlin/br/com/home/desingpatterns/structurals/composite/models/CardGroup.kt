package br.com.home.desingpatterns.structurals.composite.models

class CardGroup(override val cardName: String, override val cardType: CardTypeEnum) : CardList(cardName, cardType) {
    var cardList: List<CardList> = mutableListOf()

    fun addCardList(cardList: CardList) {
        this.cardList = this.cardList.plus(cardList)
    }

    override fun getPercentageDone(): Double {
        return cardList.sumOf { it.getPercentageDone() } / cardList.size
    }

}