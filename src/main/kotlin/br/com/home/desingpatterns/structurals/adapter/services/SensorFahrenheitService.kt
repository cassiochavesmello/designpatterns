package br.com.home.desingpatterns.structurals.adapter.services

class SensorFahrenheitService {

    fun getSensorTemperature(): Float{
        return 50f
    }
}