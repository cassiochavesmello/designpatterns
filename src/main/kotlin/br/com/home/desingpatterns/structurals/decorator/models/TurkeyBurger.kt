package br.com.home.desingpatterns.structurals.decorator.models

class TurkeyBurger : IFood {
    override fun getCost() : Double {
        return 22.50
    }
}