package br.com.home.desingpatterns.structurals.decorator.models

class CheeseDecorator(food : IFood) : AddonDecorator(food) {
    override fun getCost(): Double {
        return super.getCost() + 3.50
    }
}