package br.com.home.desingpatterns.structurals.bridge.models

class FinancialSector: Resource {

    override fun getSectorName(): String {
        return "Financial"
    }

    override fun getActualData(): String {
        return "New data from financial sector"
    }

    override fun getHistoricalData(): String {
        return "Historical data from financial sector"
    }
}