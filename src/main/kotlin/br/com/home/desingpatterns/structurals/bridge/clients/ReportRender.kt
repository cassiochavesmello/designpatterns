package br.com.home.desingpatterns.structurals.bridge.clients

import br.com.home.desingpatterns.structurals.bridge.models.*

class ReportRender {

    fun renderReportData() {
        val financialResource = FinancialSector()
        val marketingResource = MarketingSector()

        val financialAnalyticalReport = AnalyticalReport(financialResource)
        val financialSummaryReport = SummaryReport(financialResource)
        val marketingAnalyticalReport = AnalyticalReport(marketingResource)
        val marketingSummaryReport = SummaryReport(marketingResource)

        financialAnalyticalReport.render()
        financialSummaryReport.render()
        marketingAnalyticalReport.render()
        marketingSummaryReport.render()
    }
}