package br.com.home.desingpatterns.structurals.composite.models

abstract class CardList(open val cardName: String, open val cardType: CardTypeEnum) {
    private var currentStatus: CardStatusEnum = CardStatusEnum.TO_DO

    abstract fun getPercentageDone(): Double

    open fun setDone() {
        currentStatus = CardStatusEnum.DONE
    }
}