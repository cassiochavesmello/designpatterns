package br.com.home.desingpatterns.structurals.adapter.services

import org.springframework.stereotype.Service

@Service
class SensorKelvinService {

    fun getSensorTemperature(): Float{
        return 25f
    }
}