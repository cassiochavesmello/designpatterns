package br.com.home.desingpatterns.structurals.decorator.clients

import br.com.home.desingpatterns.structurals.decorator.models.*

class CostCalculator {
    fun evaluateTotalCost() {
        var order : IFood

        order = CheeseBurger()
        order = EggDecorator(order)
        order = CheeseDecorator(order)
        println("Total cost of Cheese Burger with Egg and Cheese: " + order.getCost())

        order = TurkeyBurger()
        order = CheeseDecorator(order)
        println("Total cost of Turkey Burger with Cheese: " + order.getCost())
    }
}