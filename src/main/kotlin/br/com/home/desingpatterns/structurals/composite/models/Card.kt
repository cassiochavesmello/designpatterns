package br.com.home.desingpatterns.structurals.composite.models

class Card(override val cardName: String, override val cardType: CardTypeEnum) : CardList(cardName, cardType) {
    var percDone: Double = 0.0

    override fun getPercentageDone(): Double {
        return percDone
    }

    override fun setDone() {
        percDone = 100.0
        super.setDone()
    }
}