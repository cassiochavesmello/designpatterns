package br.com.home.desingpatterns.structurals.composite.models

enum class CardStatusEnum {
    TO_DO,
    DOING,
    DONE
}