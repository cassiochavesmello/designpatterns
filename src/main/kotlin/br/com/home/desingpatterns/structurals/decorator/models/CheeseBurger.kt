package br.com.home.desingpatterns.structurals.decorator.models

class CheeseBurger: IFood {
    override fun getCost() : Double {
        return 25.70
    }
}