package br.com.home.desingpatterns.structurals.adapter.clients

import br.com.home.desingpatterns.structurals.adapter.services.SensorFahrenheitService
import br.com.home.desingpatterns.structurals.adapter.services.SensorKelvinService
import br.com.home.desingpatterns.structurals.adapter.services.adapters.SensorFahrenheitToCelciusAdapterService
import br.com.home.desingpatterns.structurals.adapter.services.adapters.SensorKelvinToCelciusAdapterService

class TemperatureReader {

    fun readActualTemperature() {
        val temperatureSensorAService = SensorKelvinToCelciusAdapterService(SensorKelvinService())
        println("Sensor A actual temperature in Celsius: " + temperatureSensorAService.getCelsiusTemperature())

        val temperatureSensorBService = SensorFahrenheitToCelciusAdapterService(SensorFahrenheitService())
        println("Sensor B actual temperature in Celsius: " + temperatureSensorBService.getCelsiusTemperature())
    }
}