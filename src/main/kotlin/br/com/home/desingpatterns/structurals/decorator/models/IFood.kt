package br.com.home.desingpatterns.structurals.decorator.models

interface IFood {
    fun getCost() : Double
}