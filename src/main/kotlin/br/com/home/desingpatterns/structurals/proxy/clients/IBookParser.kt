package br.com.home.desingpatterns.structurals.proxy.clients

interface IBookParser {

    fun getNumPages(): Int
}