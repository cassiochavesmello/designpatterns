package br.com.home.desingpatterns.structurals.proxy.services

import br.com.home.desingpatterns.structurals.proxy.clients.IBookParser

class LazyCachedBookParserProxy(val bookName: String): IBookParser {

    private var bookParser: IBookParser? = null

    override fun getNumPages(): Int {
        parseBook()
        return this.bookParser?.getNumPages() ?: 0
    }

    private fun parseBook() {
        if(bookParser == null){
            this.bookParser = BookParser(bookName)
        }
    }
}