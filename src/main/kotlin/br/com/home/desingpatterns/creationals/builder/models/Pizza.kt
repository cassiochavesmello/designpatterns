package br.com.home.desingpatterns.creationals.builder.models

class Pizza {
    var name: String = ""
    var stuffedCrust: Boolean = false
    var toppings: List<ProductsEnum> = mutableListOf()
}