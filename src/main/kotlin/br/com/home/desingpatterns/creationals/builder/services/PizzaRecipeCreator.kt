package br.com.home.desingpatterns.creationals.builder.services

import br.com.home.desingpatterns.creationals.builder.builders.PizzaDirector
import br.com.home.desingpatterns.creationals.builder.models.Pizza

class PizzaRecipeCreator {

    fun createPizzas(){
        val pizzaDirector = PizzaDirector()

        val pizzaPepperoniWithStuffedCrust = pizzaDirector.makePepperoni(true)
        printPizza(pizzaPepperoniWithStuffedCrust)

        val pizzaCheeseWithStuffedCrust = pizzaDirector.makeCheese(true)
        printPizza(pizzaCheeseWithStuffedCrust)

        val pizzaChickenWithoutStuffedCrust = pizzaDirector.makeChicken(false)
        printPizza(pizzaChickenWithoutStuffedCrust)
    }

    private fun printPizza(pizza: Pizza){
        print(pizza.name + " created ")
        if(pizza.toppings.isNotEmpty()){
            print(pizza.toppings.joinToString(", ", "with "))
        }
        if(pizza.stuffedCrust){
            print(" and stuffed crust")
        }
        println()
    }
}