package br.com.home.desingpatterns.creationals.abstractfactory.factories.deliverymethod

import br.com.home.desingpatterns.creationals.abstractfactory.models.transport.*

class MotorcycleDeliveryFactory: IDeliveryFactory {
    override fun createUberDelivery(): IUberTransport {
        return MotorcycleUber()
    }

    override fun createFlashDelivery(): IFlashTransport {
        return MotorcycleFlash()
    }
}