package br.com.home.desingpatterns.creationals.builder.builders

import br.com.home.desingpatterns.creationals.builder.models.Pizza
import br.com.home.desingpatterns.creationals.builder.models.ProductsEnum

class PizzaBuilder {
    private var pizza: Pizza = Pizza()

    private fun reset(){
        pizza = Pizza()
    }

    fun setName(name: String){
        pizza.name = name
    }

    fun addCheese(){
        pizza.toppings = pizza.toppings.plus(ProductsEnum.CHEESE)
    }

    fun addPepperoni(){
        pizza.toppings = pizza.toppings.plus(ProductsEnum.PEPPERONNI)
    }

    fun addChicken(){
        pizza.toppings = pizza.toppings.plus(ProductsEnum.CHICKEN)
    }

    fun addStuffedCrust(){
        pizza.stuffedCrust = true
    }

    fun getResult(): Pizza{
        val pizzaResult = pizza
        reset()
        return pizzaResult
    }

}