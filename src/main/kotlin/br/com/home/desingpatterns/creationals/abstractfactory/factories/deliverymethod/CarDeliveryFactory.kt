package br.com.home.desingpatterns.creationals.abstractfactory.factories.deliverymethod

import br.com.home.desingpatterns.creationals.abstractfactory.models.transport.CarFlash
import br.com.home.desingpatterns.creationals.abstractfactory.models.transport.CarUber
import br.com.home.desingpatterns.creationals.abstractfactory.models.transport.IFlashTransport
import br.com.home.desingpatterns.creationals.abstractfactory.models.transport.IUberTransport

class CarDeliveryFactory: IDeliveryFactory {
    override fun createUberDelivery(): IUberTransport {
        return CarUber()
    }

    override fun createFlashDelivery(): IFlashTransport {
        return CarFlash()
    }
}