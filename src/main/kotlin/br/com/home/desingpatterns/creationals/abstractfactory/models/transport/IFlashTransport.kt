package br.com.home.desingpatterns.creationals.abstractfactory.models.transport

interface IFlashTransport {
    fun estimateCost(): Float
}