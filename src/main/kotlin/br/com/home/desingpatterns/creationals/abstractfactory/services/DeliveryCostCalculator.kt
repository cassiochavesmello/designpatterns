package br.com.home.desingpatterns.creationals.abstractfactory.services

import br.com.home.desingpatterns.creationals.abstractfactory.factories.deliverymethod.CarDeliveryFactory
import br.com.home.desingpatterns.creationals.abstractfactory.factories.deliverymethod.IDeliveryFactory
import br.com.home.desingpatterns.creationals.abstractfactory.factories.deliverymethod.MotorcycleDeliveryFactory
import br.com.home.desingpatterns.creationals.abstractfactory.models.transport.IFlashTransport
import br.com.home.desingpatterns.creationals.abstractfactory.models.transport.IUberTransport

class DeliveryCostCalculator {

    fun evaluateDeliveryCost() {
        val carDeliveryMethod: IDeliveryFactory = getDelivery("CAR")

        val carUber: IUberTransport = carDeliveryMethod.createUberDelivery()
        println( "Uber cost by car: " + carUber.estimateCost() )

        val carFlash: IFlashTransport = carDeliveryMethod.createFlashDelivery()
        println( "Flash cost by car: " + carFlash.estimateCost() )

        val motorcycleDeliveryMethod: IDeliveryFactory = getDelivery("MOTORCYCLE")

        val motorcycleUber: IUberTransport = motorcycleDeliveryMethod.createUberDelivery()
        println( "Uber cost by motorcycle: " + motorcycleUber.estimateCost() )

        val motorcycleFlash: IFlashTransport = motorcycleDeliveryMethod.createFlashDelivery()
        println( "Flash cost by motorcycle: " + motorcycleFlash.estimateCost() )
    }

    private fun getDelivery(transport: String): IDeliveryFactory {
        return when (transport) {
            "CAR" -> {
                CarDeliveryFactory()
            } else -> {
                MotorcycleDeliveryFactory()
            }
        }
    }
}