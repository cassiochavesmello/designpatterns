package br.com.home.desingpatterns.creationals.abstractfactory.models.transport

interface IUberTransport {
    fun estimateCost(): Float
}