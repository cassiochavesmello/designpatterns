package br.com.home.desingpatterns.creationals.factorymethod.models.transport

class Motorcycle: ITransport {

    override fun getAverageSpeedInKmPerHour(): Float {
        return 15F
    }
}