package br.com.home.desingpatterns.creationals.abstractfactory.models.transport

class CarUber: IUberTransport {
    override fun estimateCost(): Float {
        return 50f
    }
}