package br.com.home.desingpatterns.creationals.factorymethod.shipping

import br.com.home.desingpatterns.creationals.factorymethod.models.transport.Car
import br.com.home.desingpatterns.creationals.factorymethod.models.transport.ITransport

class CarShipping: Shipping() {
    override fun createTransport(): ITransport {
        return Car()
    }
}