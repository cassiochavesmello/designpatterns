package br.com.home.desingpatterns.creationals.factorymethod.shipping

import br.com.home.desingpatterns.creationals.factorymethod.models.transport.ITransport
import br.com.home.desingpatterns.creationals.factorymethod.models.transport.Motorcycle

class MotorcycleShipping: Shipping() {
    override fun createTransport(): ITransport {
        return Motorcycle()
    }
}