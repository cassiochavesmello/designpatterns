package br.com.home.desingpatterns.creationals.abstractfactory.factories.deliverymethod

import br.com.home.desingpatterns.creationals.abstractfactory.models.transport.IFlashTransport
import br.com.home.desingpatterns.creationals.abstractfactory.models.transport.IUberTransport

interface IDeliveryFactory {
    fun createUberDelivery(): IUberTransport
    fun createFlashDelivery(): IFlashTransport
}