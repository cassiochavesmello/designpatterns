package br.com.home.desingpatterns.creationals.factorymethod.models.transport

class Car() : ITransport {

    override fun getAverageSpeedInKmPerHour(): Float {
        return 20.5F
    }
}