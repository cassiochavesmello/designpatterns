package br.com.home.desingpatterns.creationals.factorymethod.models.transport

interface ITransport {
    fun getAverageSpeedInKmPerHour(): Float
}