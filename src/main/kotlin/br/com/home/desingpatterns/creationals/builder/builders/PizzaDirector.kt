package br.com.home.desingpatterns.creationals.builder.builders

import br.com.home.desingpatterns.creationals.builder.models.Pizza

class PizzaDirector {
    fun makePepperoni(stuffedCrust: Boolean): Pizza {
        val pizzaPepperoni = PizzaBuilder()
        pizzaPepperoni.setName("Pepperoni Pizza")
        pizzaPepperoni.addPepperoni()
        if(stuffedCrust) {
            pizzaPepperoni.addStuffedCrust()
        }
        return pizzaPepperoni.getResult()
    }

    fun makeCheese(stuffedCrust: Boolean): Pizza {
        val pizzaCheese = PizzaBuilder()
        pizzaCheese.setName("Cheese Pizza")
        pizzaCheese.addCheese()
        if(stuffedCrust) {
            pizzaCheese.addStuffedCrust()
        }
        return pizzaCheese.getResult()
    }

    fun makeChicken(stuffedCrust: Boolean): Pizza {
        val pizzaChicken = PizzaBuilder()
        pizzaChicken.setName("Chicken Pizza")
        pizzaChicken.addChicken()
        pizzaChicken.addCheese()
        if(stuffedCrust) {
            pizzaChicken.addStuffedCrust()
        }
        return pizzaChicken.getResult()
    }

}