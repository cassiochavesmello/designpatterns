package br.com.home.desingpatterns.creationals.abstractfactory.models.transport

class CarFlash: IFlashTransport {
    override fun estimateCost(): Float {
        return 40f
    }
}