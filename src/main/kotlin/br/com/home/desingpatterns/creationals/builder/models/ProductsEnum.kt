package br.com.home.desingpatterns.creationals.builder.models

enum class ProductsEnum(name: String) {
    CHEESE("Cheese"),
    PEPPERONNI("Pepperoni"),
    CHICKEN("Chicken")
}