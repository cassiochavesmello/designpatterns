package br.com.home.desingpatterns.creationals.factorymethod.shipping

import br.com.home.desingpatterns.creationals.factorymethod.models.transport.ITransport

abstract class Shipping {

    abstract fun createTransport(): ITransport

    fun getEstimateDeliveryTimeInHours(distanceInKm: Float): Float {
        val transport = createTransport()
        return distanceInKm / transport.getAverageSpeedInKmPerHour()
    }

}