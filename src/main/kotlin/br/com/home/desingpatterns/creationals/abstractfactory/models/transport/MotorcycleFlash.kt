package br.com.home.desingpatterns.creationals.abstractfactory.models.transport

class MotorcycleFlash: IFlashTransport {
    override fun estimateCost(): Float {
        return 10f
    }
}