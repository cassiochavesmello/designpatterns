package br.com.home.desingpatterns.creationals.abstractfactory.models.transport

class MotorcycleUber: IUberTransport {
    override fun estimateCost(): Float {
        return 20f
    }
}