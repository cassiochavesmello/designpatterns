package br.com.home.desingpatterns.creationals.factorymethod.services

import br.com.home.desingpatterns.creationals.factorymethod.shipping.CarShipping
import br.com.home.desingpatterns.creationals.factorymethod.shipping.MotorcycleShipping

class DeliveryTimeCalculator {

    fun evaluateDeliveryTime() {
        val shipping = CarShipping()
        println("Car: " + shipping.getEstimateDeliveryTimeInHours(1000F))

        val shipping2 = MotorcycleShipping()
        println("Motorcycle: " + shipping2.getEstimateDeliveryTimeInHours(1000F))
    }
}